import { ModuleWithProviders } from '@angular/core';
import { MainComponent } from './main/main.component';
import { LoginComponent } from "./authen/login/login.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { RouterModule, Routes } from "@angular/router";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {UserInfoComponent} from "./user-info/user-info.component";
import {UploadProductComponent} from "./upload-product/upload-product.component";
import {ProductsComponent} from "./products/products.component";

const AppRoutes: Routes = [
  { path: '', redirectTo: 'user-info', pathMatch: 'full' },
  {
    path: '',
    component: MainComponent,
    children: [
      { path: 'user-info', component: UserInfoComponent },
      { path: 'upload-product', component: UploadProductComponent },
      { path: 'products', component: ProductsComponent },
    ],
  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: NotFoundComponent },
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
