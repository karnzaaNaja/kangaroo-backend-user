import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Configuration } from "../../config/app.constant";

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  actionUrl: any;

  constructor(
    private http: HttpClient,
    private configuration: Configuration
  ) { }

  public getUser() {
    this.actionUrl = this.configuration.ServerWithApiUrl + 'users';
    return this.http.get(this.actionUrl);
  }

  public getUserById(id) {
    this.actionUrl = this.configuration.ServerWithApiUrl + 'users/' + id;
    return this.http.get(this.actionUrl);
  }

  public updateProfile(data, id) {
    this.actionUrl = this.configuration.ServerWithApiUrl + 'users/' + id;
    return this.http.put(this.actionUrl, data);
  }
}
