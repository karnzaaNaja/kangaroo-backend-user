import { Component, OnInit } from '@angular/core';
import { UserInfoService } from "./user-info.service";

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  data: any;
  error: any;
  profile: any;

  constructor(
    private userInfo: UserInfoService,
  ) { }

  ngOnInit(): void {
    this.getUserById();
  }

  public getUserById() {
    this.userInfo.getUserById(2).subscribe(
      response => {
        this.data = response;
        if (this.data.success) {
          this.profile = this.data.data;
        }
      }, error => {
        this.error = error;
      }
    )
  }

  public onSubmitEditProfile() {
    this.userInfo.updateProfile(this.profile, this.profile.id).subscribe(
      response => {
        console.log(response)
      }, error => {
        console.log(error)
      }
    )
  }

}
